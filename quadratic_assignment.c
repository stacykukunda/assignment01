#include <stdio.h>

#include <math.h>

int main(){
    // variables
    int a,b,c;
    // for a
    printf("enter the value of a:\n");
    scanf("%d",&a);
    // validate for the value of a
    if (a==0){
        printf("the value of a must not be 0 to make  it a valid eqn\n");
    }else{
        printf("enter the value of b:\n");
        scanf("%d",&b);

        printf("enter the value of c:\n");
        scanf("%d",&c);

        // discriminant
        // (b*b)-(4*a*c)

        double discriminant=(b*b)-(4*a*c);

        if (discriminant>0){
            printf("these are real and distinct roots\n");
            double root1=(-b+(sqrt(discriminant)))/(2*a);
            double root2=(-b-(sqrt(discriminant)))/(2*a);
            printf("root1= %.4f\n",root1);
            printf("root2= %.4f\n",root2);

        }else if(discriminant==0){
            printf("the roots are real and similar\n");
            double root=-b/(2*a);
            printf("root= %.4f\n",root);

        }else{
            printf("these are complex\n");
            double realPart=-b/(2.0*a);
            double imaginaryPart=sqrt(-discriminant)/(2.0*a);
            printf("the real part is %.4f\n",realPart);
            printf("the imaginary part is %.4fi\n",imaginaryPart);
            printf("root1= %.4f + %.4fi\n",realPart,imaginaryPart);
            printf("root2= %.4f - %.4fi\n",realPart,imaginaryPart);
        }
        }
    







    return 0;
}